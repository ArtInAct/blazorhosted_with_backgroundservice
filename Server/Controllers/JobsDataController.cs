﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using yshService.Server.Models;
using yshService.Shared;

namespace yshService.Server.Controllers
{
    [AllowAnonymous]
    [ApiController]
    [Route("[controller]")]
    public class JobsDataController : ControllerBase
    {
        private yshCleanupOptions _opt = null;

        public JobsDataController(yshCleanupOptions opt) : base()
        {
            _opt = opt;
        }

        [HttpGet]
        public PurgeRequestModel Get()
        {
            return new PurgeRequestModel();
        }


        [HttpPost("Configure")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult ConfigureJobs([FromBody] PurgeRequestModel data)
        {
            return Ok();
        }

    }
}
