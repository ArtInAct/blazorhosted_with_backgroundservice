﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace yshService.Server.Models
{
    public class ApplicationUser : IdentityUser
    {
        [Required]
        [Column(TypeName = "varchar(100)")]
        public String FirstName { get; set; }

        [Required]
        [Column(TypeName = "varchar(100)")]
        public String LastName { get; set; }
    }
}
