﻿using Hangfire;
using Hangfire.Storage;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using yshService.Shared;

namespace yshService.Server.Models
{
    public class yshCleanupOptions
    {
        public String yshDB { get; set; }

        public yshCleanupOptions() { }
        public yshCleanupOptions(IConfiguration c)
        {
            yshDB = c.GetValue<String>("ConnectionStrings:yshDB");
        }

        public static JobState Status (PurgeRequests jobtype)
        {
            var jobname = Enum.GetName(typeof(PurgeRequests), jobtype);
            using (var connection = JobStorage.Current.GetConnection())
            {
                string lastRunResult = string.Empty;
                var recurringJobs = connection.GetRecurringJobs();
                
                var job = recurringJobs.FirstOrDefault(p => p.Id.Equals(jobname, StringComparison.InvariantCultureIgnoreCase));
                if (job != null)
                {
                    try
                    {
                        var jobState = connection.GetStateData(job.LastJobId);
                        lastRunResult = jobState.Name; // "Succeeded", "Processing", "Deleted"
                        if (lastRunResult.Equals("Succeeded", StringComparison.CurrentCultureIgnoreCase))
                            return JobState.Started;

                        if (lastRunResult.Equals("Deleted", StringComparison.CurrentCultureIgnoreCase))
                            return JobState.Stopped;
                    }
                    catch (Exception)
                    {
                        return JobState.Unspecified;
                    }
                }
            }
            return JobState.Stopped;

        }
        



    }
}
