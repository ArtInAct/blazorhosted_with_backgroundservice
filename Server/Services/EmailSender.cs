﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.UI.Services; 


namespace yshService.Server.Services
{
    public class EmailSender : IEmailSender
    {
        private string fromwhom;
        private string host;
        private int port;
        private bool enableSSL;
        private string userName;
        private string password;

        public EmailSender(String fromwhom, string host, int port, bool enableSSL, string userName, string password)
        {
            this.fromwhom= fromwhom;
            this.host = host;
            this.port = port;
            this.enableSSL = enableSSL;
            this.userName = userName;
            this.password = password;
        }

        public async Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            var client = new SmtpClient(host, port)
            {
                Credentials = new NetworkCredential(userName, password),
                EnableSsl = enableSSL
            };

            MailMessage mm = new MailMessage(fromwhom, email, subject, htmlMessage) { IsBodyHtml = true }; 

            try
            {
                await client.SendMailAsync(mm);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
