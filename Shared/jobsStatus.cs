﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace yshService.Shared
{
    public enum JobState { Started,  Stopped, Unspecified }

    public enum PurgeRequests { jobIncomplete, jobComplete, jobCancelled }


    public class PurgeRequestModel
    {
        public PurgeRequestModel(JobState inc, JobState comp, JobState canc)
        {
            Incomplete = inc;
            Complete = comp;
            Cancelled = canc;

        }

        public PurgeRequestModel()
        {
            Incomplete = JobState.Unspecified;
            Complete = JobState.Unspecified;
            Cancelled = JobState.Unspecified;
        }

        [Required]
        public JobState Incomplete { get; set; }

        [Required]
        public JobState Complete { get; set; }

        [Required]
        public JobState Cancelled { get; set; }
    }
}
